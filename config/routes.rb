Rails.application.routes.draw do
  root "doctors#index"
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :patients, only: [:index, :show, :new, :create]

end
