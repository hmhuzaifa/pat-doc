ActiveAdmin.register Doctor do

  permit_params :email, :password, :password_confirmation, :first_name, :last_name, :specialization

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :specialization
    column :email
    actions
  end

  filter :email
  filter :first_name
  filter :last_name

  form do |f|
    f.inputs do
      f.input :first_name
      f.input :last_name
      f.input :specialization
      f.input :email
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
