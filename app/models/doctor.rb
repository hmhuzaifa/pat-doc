class Doctor < User
  has_many :patients
  validates :email, presence: true
  validates :password, presence: true
  validates :specialization, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
end
