class Patient < User
  belongs_to :doctor
  validates :email, presence: true,format: { with: /\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ }
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :age, presence: true
  validates :disease, presence: true
  validates :phone_number, presence: true
  validates :address, presence: true
  validates :gender, presence: true
end