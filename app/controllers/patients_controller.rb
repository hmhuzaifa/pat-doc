class PatientsController < ApplicationController
  def index
    @doctor = Doctor.find(current_user.id)
    @total = @doctor.patients.count
    @patients = @doctor.patients
    @patients = @patients.where('first_name = ?', params[:search]) if params[:search].present?
  end

  def show
    @patient = Patient.find(params[:id])
  end

  def new
    @patient = Patient.new
  end

  def create
    @doctor = Doctor.find(current_user.id)
    @patient = @doctor.patients.new(patient_params)
    if @patient.save
      redirect_to patient_path(@patient), status: :see_other
    else
      render :new, status: :unprocessable_entity
    end
  end

  private

  def patient_params
    params.require(:patient).permit(:first_name, :last_name, :email, :age, :disease, :gender, :phone_number, :address)
  end
end
