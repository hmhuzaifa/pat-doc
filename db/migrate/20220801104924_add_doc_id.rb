class AddDocId < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :doctor, :integer
    add_reference :users, :doctor, foreign_keys: { to_table: :users }, index: true
  end
end
