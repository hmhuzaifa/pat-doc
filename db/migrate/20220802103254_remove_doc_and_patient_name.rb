class RemoveDocAndPatientName < ActiveRecord::Migration[7.0]
  def change
    remove_column :users, :doc_Name
    remove_column :users, :patient_name
  end
end
