class AddAttrToUserForDoctor < ActiveRecord::Migration[7.0]
  def change
    add_column :users ,:doc_Name, :string
    add_column :users ,:specialization, :string
  end
end
