class AddAttrToUserForPatient < ActiveRecord::Migration[7.0]
  def change
    add_column :users, :patient_name, :string
    add_column :users, :age, :integer
    add_column :users, :disease, :string
    add_column :users, :gender, :string
    add_column :users, :phone_number, :string
    add_column :users, :p_address, :string
  end
end
