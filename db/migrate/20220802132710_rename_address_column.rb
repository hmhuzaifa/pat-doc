class RenameAddressColumn < ActiveRecord::Migration[7.0]
  def change
    rename_column :users, :patient_address, :address
  end
end
